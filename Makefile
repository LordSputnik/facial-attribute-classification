
data/originals/index: scripts/index.py data/originals
	./scripts/index.py data/originals data/originals

data/align: data/originals/index scripts/align.py
	rm -Rf data/align
	mkdir data/align
	./scripts/align.py data/originals data/align

data/asm: data/align scripts/asm.py
	rm -Rf data/asm
	mkdir data/asm
	./scripts/asm.py data/align data/asm

data/warp: data/asm scripts/warp.py
	rm -Rf data/warp
	mkdir data/warp
	./scripts/warp.py data/asm data/warp

data/colour: data/warp scripts/colour.py
	rm -Rf data/colour
	mkdir data/colour
	./scripts/colour.py data/warp data/colour

data/aam: data/asm data/colour scripts/aam.py
	rm -Rf data/aam
	mkdir data/aam
	./scripts/aam.py data/colour data/aam

data/cnn: data/colour data/aam scripts/cnn.py
	rm -Rf data/cnn
	mkdir data/cnn
	./scripts/cnn.py data/aam data/cnn

data/gabor: data/cnn scripts/gabor.py
	rm -Rf data/gabor
	mkdir data/gabor
	./scripts/gabor.py data/cnn data/gabor

data/lbp: data/gabor scripts/lbp.py
	rm -Rf data/lbp
	mkdir data/lbp
	./scripts/lbp.py data/gabor data/lbp

data/features: data/lbp scripts/features.py
	rm -Rf data/features
	mkdir data/features
	./scripts/features.py data/lbp data/features

data/genetic: data/features scripts/genetic_classify.py
	rm -Rf data/genetic
	mkdir data/genetic
	./scripts/genetic_classify.py data/features data/genetic

results:
	mkdir results

results/%: data/features scripts/classify.py results
	rm -Rf $@
	mkdir $@
	./scripts/classify.py data/features $@

.phony: clean

clean:
	rm -f data/originals/index
	rm -Rf data/align
	rm -Rf data/asm
	rm -Rf data/warp
	rm -Rf data/aam
	rm -Rf data/cnn
	rm -Rf data/features
