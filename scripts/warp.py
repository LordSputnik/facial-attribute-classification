#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script reads the points files from the index in the specified input
directory, and also the mean points file from the same index. It then warps the
images in the index file to the mean image shape.
"""

from os.path import abspath, isdir, join

import cv2

import numpy as np
from OpenGL import GL, GLUT
from OpenGL.GL import (GL_BGR, GL_BGRA, GL_CLAMP_TO_BORDER, GL_COLOR_BUFFER_BIT,
                       GL_FRONT_AND_BACK, GL_LINE, GL_LINEAR, GL_POINT,
                       GL_RGB16, GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                       GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S,
                       GL_TEXTURE_WRAP_T, GL_TRIANGLES, GL_UNSIGNED_BYTE,
                       glBegin, glBindTexture, glClear, glDeleteTextures,
                       glEnd, glPolygonMode, glReadPixels, glTexCoord,
                       glVertex)
from OpenGL.GLUT import GLUT_DEPTH, GLUT_DOUBLE, GLUT_RGBA, GLUT_ALPHA
from scipy.spatial import Delaunay

from index import load_index, save_index
from util import load_image, load_points, save_image

def _draw_face(vertex_points, texture_points, triangles):
    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
    GL.glEnd()

def _draw_left_eye(vertex_points, texture_points, triangles):
    include = [10, 11, 27, 50, 51]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_left_cheek(vertex_points, texture_points, triangles):
    include = [8, 9, 20, 21, 22, 43, 44]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_right_eye(vertex_points, texture_points, triangles):
    include = [2, 3, 4, 15, 16]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum


def _draw_right_cheek(vertex_points, texture_points, triangles):
    include = [12, 13, 14, 31, 64, 65, 66]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum


def _draw_left_under_eye(vertex_points, texture_points, triangles):
    include = [9, 11, 43, 44]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_right_under_eye(vertex_points, texture_points, triangles):
    include = [3, 12, 13, 31]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum


def _draw_right_mouth(vertex_points, texture_points, triangles):
    include = [29, 63, 64, 65, 66]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_left_mouth(vertex_points, texture_points, triangles):
    include = [20, 21, 22, 24, 25]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_left_mouth(vertex_points, texture_points, triangles):
    include = [20, 21, 22, 24, 25]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_left_nose(vertex_points, texture_points, triangles):
    include = [8, 9, 21]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_right_nose(vertex_points, texture_points, triangles):
    include = [13, 14, 66]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    _draw_left_nose(vertex_points, texture_points, triangles)
    triangles = triangles[include]
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_top_nose(vertex_points, texture_points, triangles):
    include = [6, 7, 26, 67, 68, 69]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    triangles = triangles[include]
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def _draw_bridge_nose(vertex_points, texture_points, triangles):
    include = [40, 41, 60, 68, 69, 70, 94, 101, 103]
    triangles = triangles[include]

    GL.glClear(GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL_TRIANGLES)
    vertices = []
    for tri in triangles:
        t0 = texture_points[tri[0]]
        t1 = texture_points[tri[1]]
        t2 = texture_points[tri[2]]

        GL.glTexCoord(*t0)
        GL.glVertex(*vertex_points[tri[0]])
        vertices.append(vertex_points[tri[0]])

        GL.glTexCoord(*t1)
        GL.glVertex(*vertex_points[tri[1]])
        vertices.append(vertex_points[tri[1]])

        GL.glTexCoord(*t2)
        GL.glVertex(*vertex_points[tri[2]])
        vertices.append(vertex_points[tri[2]])
    GL.glEnd()

    minimum = ((np.min(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    maximum = ((np.max(vertices, axis=0) * 256.0) + 256.0).astype(np.uint16)
    return minimum, maximum

def warp(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    GLUT.glutInit()
    GLUT.glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH)
    GLUT.glutInitWindowSize(512, 512)
    GLUT.glutInitWindowPosition(0, 0)
    window = GLUT.glutCreateWindow("Warp")

    GL.glEnable(GL_TEXTURE_2D)
    GL.glClearColor(0.0, 0.0, 0.0, 0.0)
    GL.glScale(1.0, -1.0, 1.0)

    index = load_index(dir_in)

    vertex_points = load_points(index['mean']['shape']) / 2
    triangles = Delaunay(vertex_points).simplices

    for entry in index['faces']:
        # Load image and create texture
        image = load_image(entry['texture'])
        texture = GL.glGenTextures(1)
        GL.glBindTexture(GL_TEXTURE_2D, texture)

        GL.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER)
        GL.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
        GL.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        GL.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        GL.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, image.shape[0], image.shape[1],
                        0, GL_BGR, GL_UNSIGNED_BYTE, image)

        texture_points = (load_points(entry['shape']) + 2.0) / 4.0

        minimum, maximum = _draw_left_eye(vertex_points, texture_points, triangles)
        left_eye_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_right_eye(vertex_points, texture_points, triangles)
        right_eye_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_left_under_eye(vertex_points, texture_points, triangles)
        left_under_eye_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_right_under_eye(vertex_points, texture_points, triangles)
        right_under_eye_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_left_mouth(vertex_points, texture_points, triangles)
        left_mouth_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_right_mouth(vertex_points, texture_points, triangles)
        right_mouth_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_left_nose(vertex_points, texture_points, triangles)
        left_nose_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_right_nose(vertex_points, texture_points, triangles)
        right_nose_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_top_nose(vertex_points, texture_points, triangles)
        top_nose_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_bridge_nose(vertex_points, texture_points, triangles)
        bridge_nose_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_left_cheek(vertex_points, texture_points, triangles)
        left_cheek_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        minimum, maximum = _draw_right_cheek(vertex_points, texture_points, triangles)
        right_cheek_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)[minimum[1]:maximum[1], minimum[0]:maximum[0]]

        _draw_face(vertex_points, texture_points, triangles)
        face_array = cv2.flip(GL.glReadPixels(
            0, 0, 512, 512, GL_BGRA, GL_UNSIGNED_BYTE, outputType=np.array
        ), 0)

        GL.glBindTexture(GL_TEXTURE_2D, 0)
        GL.glDeleteTextures(texture)

        image_path_512 = abspath(join(dir_out, entry['id'] + '_512.png'))
        save_image(face_array, image_path_512)

        face_array = cv2.resize(face_array, (256, 256),
                                interpolation=cv2.INTER_LANCZOS4)
        image_path_256 = abspath(join(dir_out, entry['id'] + '.png'))
        save_image(face_array, image_path_256)

        image_path_left_eye = abspath(join(dir_out, entry['id'] + '_left_eye.png'))
        save_image(left_eye_array, image_path_left_eye)

        image_path_right_eye = abspath(join(dir_out, entry['id'] + '_right_eye.png'))
        save_image(right_eye_array, image_path_right_eye)

        image_path_left_under_eye = abspath(join(dir_out, entry['id'] + '_left_under_eye.png'))
        save_image(left_under_eye_array, image_path_left_under_eye)

        image_path_right_under_eye = abspath(join(dir_out, entry['id'] + '_right_under_eye.png'))
        save_image(right_under_eye_array, image_path_right_under_eye)

        image_path_left_mouth = abspath(join(dir_out, entry['id'] + '_left_mouth.png'))
        save_image(left_mouth_array, image_path_left_mouth)

        image_path_right_mouth = abspath(join(dir_out, entry['id'] + '_right_mouth.png'))
        save_image(right_mouth_array, image_path_right_mouth)

        image_path_left_nose = abspath(join(dir_out, entry['id'] + '_left_nose.png'))
        save_image(left_nose_array, image_path_left_nose)

        image_path_right_nose = abspath(join(dir_out, entry['id'] + '_right_nose.png'))
        save_image(right_nose_array, image_path_right_nose)

        image_path_top_nose = abspath(join(dir_out, entry['id'] + '_top_nose.png'))
        save_image(top_nose_array, image_path_top_nose)

        image_path_bridge_nose = abspath(join(dir_out, entry['id'] + '_bridge_nose.png'))
        save_image(bridge_nose_array, image_path_bridge_nose)

        image_path_left_cheek = abspath(join(dir_out, entry['id'] + '_left_cheek.png'))
        save_image(left_cheek_array, image_path_left_cheek)

        image_path_right_cheek = abspath(join(dir_out, entry['id'] + '_right_cheek.png'))
        save_image(right_cheek_array, image_path_right_cheek)

        entry['warped512'] = image_path_512
        entry['warped'] = image_path_256
        entry['warped_left_eye'] = image_path_left_eye
        entry['warped_right_eye'] = image_path_right_eye
        entry['warped_left_under_eye'] = image_path_left_under_eye
        entry['warped_right_under_eye'] = image_path_right_under_eye
        entry['warped_left_mouth'] = image_path_left_mouth
        entry['warped_right_mouth'] = image_path_right_mouth
        entry['warped_left_nose'] = image_path_left_nose
        entry['warped_right_nose'] = image_path_right_nose
        entry['warped_top_nose'] = image_path_top_nose
        entry['warped_bridge_nose'] = image_path_bridge_nose
        entry['warped_left_cheek'] = image_path_left_cheek
        entry['warped_right_cheek'] = image_path_right_cheek

    save_index(index, dir_out)

if __name__ == '__main__':
    import sys
    warp(sys.argv[1], sys.argv[2])
