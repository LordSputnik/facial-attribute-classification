#!/usr/bin/env python
# -*- coding: utf8 -*-

from os.path import abspath, isdir, join

from index import load_index, save_index
from util import load_image

import numpy as np
import math
import cv2

import sklearn.decomposition


def _filter(ksize, sigma, theta, lambd):
    kernel = cv2.getGaborKernel((ksize, ksize), sigma, theta, lambd,
                                0.3, 0, ktype=cv2.CV_32F)
    return kernel / kernel.sum()


def _pool(image, pool_size):
    half_pool_size = pool_size / 2
    x_size, y_size = (image.shape[0] / pool_size * 2 - 1 , image.shape[1] / pool_size * 2 - 1)
    pool_pixels = pool_size * pool_size
    cells = np.zeros((x_size*y_size, pool_size, pool_size))
    for y in range(y_size):
        for x in range(x_size):
            x_start = x * half_pool_size
            x_end = x_start + pool_size
            y_start = y * half_pool_size
            y_end = y_start + pool_size
            cell = image[x_start: x_end, y_start: y_end]
            cells[y*x_size + x] = cell

    result = np.std(cells, axis=(1, 2))
    return result


def _gabor_image(bands, image, orientations):
    gabor_features = []

    for i, band in enumerate(bands):
        pool_size = 6 + (2*i)
        for orientation, f12 in enumerate(band):
            if orientation not in orientations:
                continue

            f1, f2 = f12
            filtered1 = cv2.filter2D(image, cv2.CV_8U, f1)
            filtered2 = cv2.filter2D(image, cv2.CV_8U, f2)

            max_filtered = np.maximum(filtered1, filtered2)
            pooled = _pool(max_filtered, pool_size)
            gabor_features.extend(pooled.tolist())

    return gabor_features


def gabor(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)

    # Generate filter bands
    bands = [
        zip(
            [_filter(5, 2.0, t, 2.5) for t in np.arange(0, np.pi, np.pi/6)],
            [_filter(7, 2.8, t, 3.5) for t in np.arange(0, np.pi, np.pi/6)],
        ),
        zip(
            [_filter(9, 3.6, t, 4.6) for t in np.arange(0, np.pi, np.pi/6)],
            [_filter(11, 4.5, t, 5.6) for t in np.arange(0, np.pi, np.pi/6)],
        ),
        zip(
            [_filter(13, 5.4, t, 6.8) for t in np.arange(0, np.pi, np.pi/6)],
            [_filter(15, 6.3, t, 7.9) for t in np.arange(0, np.pi, np.pi/6)],
        ),
        zip(
            [_filter(17, 7.3, t, 9.1) for t in np.arange(0, np.pi, np.pi/6)],
            [_filter(19, 8.2, t, 10.3) for t in np.arange(0, np.pi, np.pi/6)],
        )
    ]

    all_gabor_features = []
    for entry in index['faces']:
        gabor_features = []
        image = cv2.cvtColor(load_image(entry['warped_left_under_eye']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (1, 2, 3)))

        image = cv2.cvtColor(load_image(entry['warped_right_under_eye']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (3, 4, 5)))

        image = cv2.cvtColor(load_image(entry['warped_left_nose']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (0, 1, 2)))

        image = cv2.cvtColor(load_image(entry['warped_left_mouth']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (0, 1, 2)))

        image = cv2.cvtColor(load_image(entry['warped_right_nose']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (0, 4, 5)))

        image = cv2.cvtColor(load_image(entry['warped_right_mouth']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (0, 4, 5)))

        image = cv2.cvtColor(load_image(entry['warped_left_eye']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (0, 1, 2, 3, 4, 5)))

        image = cv2.cvtColor(load_image(entry['warped_right_eye']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (0, 1, 2, 3, 4, 5)))

        image = cv2.cvtColor(load_image(entry['warped_top_nose']), cv2.COLOR_BGR2GRAY)
        gabor_features.extend(_gabor_image(bands, image, (3, )))

        all_gabor_features.append(gabor_features)

        print len(all_gabor_features)

    pca_builder = sklearn.decomposition.PCA(n_components=0.9999, copy=False)
    transformed = pca_builder.fit_transform(all_gabor_features)
    for i, entry in enumerate(index['faces']):
        entry['gabor'] = transformed[i].tolist()

    save_index(index, dir_out)

if __name__ == '__main__':
    import sys
    gabor(sys.argv[1], sys.argv[2])
