#!/usr/bin/env python
# -*- coding: utf8 -*-

from os.path import abspath, isdir, join
from index import load_index, save_index
from util import load_image, save_image

import cv2
import numpy as np


def _equalize_hist(image, hist, bins):
    cdf = hist.cumsum()
    cdf = 255 * cdf / cdf[-1]
    out = np.interp(image, bins[:-1], cdf)

    return out.reshape(image.shape).astype(np.uint8)


def _equalize_rgb(image):
    """ Equalize the histogram for an RGB image."""
    # First, exclude the border pixels

    r, g, b, alpha = cv2.split(image)

    r_flat = r[alpha > 0.0].flatten()
    hist, bins = np.histogram(r_flat, 256, normed=True)
    r_eq = _equalize_hist(r, hist, bins)

    g_flat = g[alpha > 0.0].flatten()
    hist, bins = np.histogram(g_flat, 256, normed=True)
    g_eq = _equalize_hist(g, hist, bins)

    b_flat = b[alpha > 0.0].flatten()
    hist, bins = np.histogram(b_flat, 256, normed=True)
    b_eq = _equalize_hist(b, hist, bins)

    return cv2.merge((r_eq, g_eq, b_eq, alpha))


def _equalize_hls(image):
    """ Equalize the histogram for an RGB image."""
    # First, exclude the border pixels

    alpha = image[:, :, 3]
    hls_image = cv2.cvtColor(image[:, :, :3], cv2.COLOR_BGR2HLS)

    h, l, s = cv2.split(hls_image)

    # Equalize only l
    l_flat = l[alpha > 0.0].flatten()
    hist, bins = np.histogram(l_flat, 256, normed=True)
    l_eq = _equalize_hist(l, hist, bins)

    merged = cv2.merge((h, l_eq, s))

    merged_bgr = cv2.cvtColor(merged, cv2.COLOR_HLS2BGR)
    return cv2.merge((merged_bgr[:, :, 0], merged_bgr[:, :, 1], merged_bgr[:, :, 2], alpha))


def _equalize_hsv(image):
    """ Equalize the histogram for an RGB image."""
    # First, exclude the border pixels

    alpha = image[:, :, 3]
    hsv_image = cv2.cvtColor(image[:, :, :3], cv2.COLOR_BGR2HSV)

    h, s, v = cv2.split(hsv_image)

    # Equalize only v
    v_flat = v[alpha > 0.0].flatten()
    hist, bins = np.histogram(v_flat, 256, normed=True)
    v_eq = _equalize_hist(v, hist, bins)

    merged = cv2.merge((h, s, v_eq))

    merged_bgr = cv2.cvtColor(merged, cv2.COLOR_HSV2BGR)
    return cv2.merge((merged_bgr[:, :, 0], merged_bgr[:, :, 1], merged_bgr[:, :, 2], alpha))


def _equalize_lab(image):
    """ Equalize the histogram for an RGB image."""
    # First, exclude the border pixels

    alpha = image[:, :, 3]
    Lab_image = cv2.cvtColor(image[:, :, :3], cv2.COLOR_BGR2LAB)

    L, a, b = cv2.split(Lab_image)

    # Equalize only v
    L_flat = L[alpha > 0.0].flatten()
    hist, bins = np.histogram(L_flat, 100, normed=True)
    L_eq = _equalize_hist(L, hist, bins)

    merged = cv2.merge((L_eq, a, b))

    merged_bgr = cv2.cvtColor(merged, cv2.COLOR_LAB2BGR)
    return cv2.merge((merged_bgr[:, :, 0], merged_bgr[:, :, 1], merged_bgr[:, :, 2], alpha))


def colour(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)

    _equalize = _equalize_lab

    for entry in index['faces']:
        image = load_image(entry['warped'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '.png'))
        save_image(_equalize(image), image_path)
        entry['warped'] = image_path

        image = load_image(entry['warped512'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_512.png'))
        save_image(_equalize(image), image_path)
        entry['warped512'] = image_path

        image = load_image(entry['warped_left_eye'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_left_eye.png'))
        save_image(_equalize(image), image_path)
        entry['warped_left_eye'] = image_path

        image = load_image(entry['warped_right_eye'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_right_eye.png'))
        save_image(_equalize(image), image_path)
        entry['warped_right_eye'] = image_path

        image = load_image(entry['warped_left_cheek'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_left_cheek.png'))
        save_image(_equalize(image), image_path)
        entry['warped_left_cheek'] = image_path

        image = load_image(entry['warped_right_cheek'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_right_cheek.png'))
        save_image(_equalize(image), image_path)
        entry['warped_right_cheek'] = image_path

        image = load_image(entry['warped_left_under_eye'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_left_under_eye.png'))
        save_image(_equalize(image), image_path)
        entry['warped_left_under_eye'] = image_path

        image = load_image(entry['warped_right_under_eye'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_right_under_eye.png'))
        save_image(_equalize(image), image_path)
        entry['warped_right_under_eye'] = image_path

        image = load_image(entry['warped_left_mouth'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_left_mouth.png'))
        save_image(_equalize(image), image_path)
        entry['warped_left_mouth'] = image_path

        image = load_image(entry['warped_right_mouth'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_right_mouth.png'))
        save_image(_equalize(image), image_path)
        entry['warped_right_mouth'] = image_path

        image = load_image(entry['warped_left_nose'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_left_nose.png'))
        save_image(_equalize(image), image_path)
        entry['warped_left_nose'] = image_path

        image = load_image(entry['warped_right_nose'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_right_nose.png'))
        save_image(_equalize(image), image_path)
        entry['warped_right_nose'] = image_path

        image = load_image(entry['warped_top_nose'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_top_nose.png'))
        save_image(_equalize(image), image_path)
        entry['warped_top_nose'] = image_path

        image = load_image(entry['warped_bridge_nose'], alpha=True)
        image_path = abspath(join(dir_out, entry['id'] + '_bridge_nose.png'))
        save_image(_equalize(image), image_path)
        entry['warped_bridge_nose'] = image_path

    save_index(index, dir_out)


def passthrough(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)

    save_index(index, dir_out)


if __name__ == '__main__':
    import sys
    passthrough(sys.argv[1], sys.argv[2])
