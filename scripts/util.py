
import cv2
import numpy as np


def load_points(filename):
    """ Load points from the file given by filename, and return them in an
    np.array.
    """
    points = []
    with open(filename, 'rb') as file_obj:
        state = 0
        for line in file_obj:
            if line.startswith("{"):
                state = 1
            elif line.startswith("}"):
                break
            elif state == 1:
                a, b = line.split()[:2]
                points.append((float(a), float(b)))

    return np.array(points)


def save_points(points, filename):
    """ Save points in points into a new file pointed to by filanem. """
    with open(filename, 'wb') as file_obj:
        file_obj.write("{\n")
        for point in points:
            file_obj.write("{} {}\n".format(str(point[0]), str(point[1])))

        file_obj.write("}\n")


def load_image(filename, alpha=False):
    """ Load an image from the specified file. """
    if alpha:
        return cv2.imread(filename, cv2.IMREAD_UNCHANGED)
    else:
        return cv2.imread(filename)


def save_image(image, filename):
    """ Save an image to the specified file. """
    return cv2.imwrite(filename, image)
