#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script reads the warped image files from the index in the specified
input directory, and uses PCA to build up a model (AAM) to describe the
variation.
"""

from os.path import abspath, join, isdir

import numpy as np
import sklearn.decomposition

from index import load_index, save_index
from util import load_image, save_image


def aam(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)
    # Load RGB of each of the input images
    features = np.array([load_image(e['warped']).flatten() for e in index['faces']])

    pca_builder = sklearn.decomposition.PCA(n_components=0.95)

    transformed_texture_features = pca_builder.fit_transform(features)
    mean = pca_builder.mean_
    mean_image = mean.reshape(256, 256, 3)

    mean_image_path = abspath(join(dir_out, 'mean.png'))
    index['mean'] = {'warped': mean_image_path}
    save_image(mean_image, mean_image_path)

    for i, entry in enumerate(index['faces']):
        entry['transformed_warped'] = transformed_texture_features[i].tolist()

    combined_features = []
    for entry in index['faces']:
        combined_features.append(entry['transformed_shape'] +
                                 entry['transformed_warped'])

    # Perform PCA on combined features
    pca_builder = sklearn.decomposition.PCA(n_components=0.95)
    transformed_combined_features = \
        pca_builder.fit_transform(combined_features)

    for i, entry in enumerate(index['faces']):
        del entry['transformed_shape']
        del entry['transformed_warped']
        entry['aam'] = transformed_combined_features[i].tolist()

    save_index(index, dir_out)

if __name__ == '__main__':
    import sys
    aam(sys.argv[1], sys.argv[2])
