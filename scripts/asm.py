#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script reads the points files from the index in the specified input
directory, and uses PCA to build up a model (ASM) to describe how points vary.
"""

from os.path import abspath, join, isdir

import numpy as np
import sklearn.decomposition

from index import load_index, save_index
from util import load_points, save_points


def _draw(points):
    import cv2
    img = np.ones((512, 512, 3), np.uint8) * 255
    points = (points * 128) + 256

    for point in points:
        print point
        cv2.circle(img, tuple(int(x) for x in point.tolist()), 2, (255, 0, 0), -1)

    for i, point in enumerate(points[:14]):
        n_point = points[i + 1]
        cv2.line(img, tuple(int(x) for x in point.tolist()), tuple(int(x) for x in n_point.tolist()),(255,0,0),1)

    for i, point in enumerate(points[27:30]):
        n_point = points[i + 28]
        cv2.line(img, tuple(int(x) for x in point.tolist()), tuple(int(x) for x in n_point.tolist()),(255,0,0),1)
    cv2.line(img, tuple(int(x) for x in points[30].tolist()), tuple(int(x) for x in points[27].tolist()),(255,0,0),1)


    for i, point in enumerate(points[32:35]):
        n_point = points[i + 33]
        cv2.line(img, tuple(int(x) for x in point.tolist()), tuple(int(x) for x in n_point.tolist()),(255,0,0),1)
    cv2.line(img, tuple(int(x) for x in points[35].tolist()), tuple(int(x) for x in points[32].tolist()),(255,0,0),1)

    for i, point in enumerate(points[48:59]):
        n_point = points[i + 49]
        cv2.line(img, tuple(int(x) for x in point.tolist()), tuple(int(x) for x in n_point.tolist()),(255,0,0),1)
    cv2.line(img, tuple(int(x) for x in points[59].tolist()), tuple(int(x) for x in points[48].tolist()),(255,0,0),1)

    print img.shape
    cv2.imshow("ASM", img)
    cv2.waitKey(0)

def asm(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)
    features = np.array([load_points(e['shape']).flatten() for e in index['faces']])

    pca_builder = sklearn.decomposition.PCA(n_components=0.95)

    transformed_shape_features = pca_builder.fit_transform(features)
    mean = pca_builder.mean_

    print transformed_shape_features.shape
    mean_transformed = np.mean(transformed_shape_features, axis=0)
    std_transformed = np.std(transformed_shape_features, axis=0)

    print transformed_shape_features[0].shape
    print mean_transformed.shape

    temp = pca_builder.inverse_transform(mean_transformed)
    print pca_builder.explained_variance_ratio_
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean.pts')

    temp = np.copy(mean_transformed)
    temp[0] += 2 * std_transformed[0]
    temp = pca_builder.inverse_transform(temp)
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean1.pts')
    _draw(points)

    temp = np.copy(mean_transformed)
    temp[0] -= 2 * std_transformed[0]
    temp = pca_builder.inverse_transform(temp)
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean-1.pts')
    _draw(points)

    temp = np.copy(mean_transformed)
    temp[1] += 2 * std_transformed[1]
    temp = pca_builder.inverse_transform(temp)
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean+2.pts')

    temp = np.copy(mean_transformed)
    temp[1] -= 2 * std_transformed[1]
    temp = pca_builder.inverse_transform(temp)
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean-2.pts')

    temp = np.copy(mean_transformed)
    temp[2] += 2 * std_transformed[2]
    temp = pca_builder.inverse_transform(temp)
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean+3.pts')

    temp = np.copy(mean_transformed)
    temp[2] -= 2 * std_transformed[2]
    temp = pca_builder.inverse_transform(temp)
    points = temp.reshape(len(temp) / 2, 2)
    save_points(points, 'asm_mean-3.pts')

    mean_points_path = abspath(join(dir_out, 'mean.pts'))
    index['mean'] = {'shape': mean_points_path}

    save_points(mean.reshape(len(mean) / 2, 2), mean_points_path)

    for i, entry in enumerate(index['faces']):
        entry['transformed_shape'] = transformed_shape_features[i].tolist()

    save_index(index, dir_out)

if __name__ == '__main__':
    import sys
    asm(sys.argv[1], sys.argv[2])
