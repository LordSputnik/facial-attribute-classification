#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script uses the final features included in the index of the input
directory to test the estimation accuracy of a particular classifier, using
cross-validation. The process is repeated 5 times, to reduce the effects of any
non-determinism in the classifier.
"""

from os.path import abspath, join, isdir

import sklearn.ensemble
import sklearn.decomposition
import sklearn.base
import sklearn.svm

from index import load_index
import numpy as np

MALE = 0
FEMALE = 1
genders = {
    "001": MALE,
    "002": FEMALE,
    "003": MALE,
    "004": MALE,
    "005": FEMALE,
    "006": MALE,
    "007": MALE,
    "008": FEMALE,
    "009": FEMALE,
    "010": FEMALE,
    "011": MALE,
    "012": FEMALE,
    "013": MALE,
    "014": FEMALE,
    "015": FEMALE,
    "016": MALE,
    "017": MALE,
    "018": FEMALE,
    "019": MALE,
    "020": FEMALE,
    "021": FEMALE,
    "022": MALE,
    "023": MALE,
    "024": MALE,
    "025": FEMALE,
    "026": FEMALE,
    "027": FEMALE,
    "028": MALE,
    "029": MALE,
    "030": FEMALE,
    "031": MALE,
    "032": FEMALE,
    "033": MALE,
    "034": FEMALE,
    "035": MALE,
    "036": MALE,
    "037": MALE,
    "038": MALE,
    "039": FEMALE,
    "040": MALE,
    "041": MALE,
    "042": MALE,
    "043": FEMALE,
    "044": MALE,
    "045": MALE,
    "046": MALE,
    "047": FEMALE,
    "048": MALE,
    "049": FEMALE,
    "050": MALE,
    "051": MALE,
    "052": FEMALE,
    "053": MALE,
    "054": FEMALE,
    "055": MALE,
    "056": MALE,
    "057": MALE,
    "058": MALE,
    "059": FEMALE,
    "060": FEMALE,
    "061": FEMALE,
    "062": FEMALE,
    "063": MALE,
    "064": MALE,
    "065": FEMALE,
    "066": MALE,
    "067": FEMALE,
    "068": MALE,
    "069": MALE,
    "070": MALE,
    "071": MALE,
    "072": FEMALE,
    "073": FEMALE,
    "074": MALE,
    "075": MALE,
    "076": FEMALE,
    "077": FEMALE,
    "078": MALE,
    "079": MALE,
    "080": MALE,
    "081": MALE,
    "082": MALE,
}

class Estimator(sklearn.base.BaseEstimator, sklearn.base.RegressorMixin):
    N_CLASSES = 4
    CLASS_OVERLAP = 0.6

    def __init__(self):
        # Coarse and detailed estimators
        self.coarse = sklearn.ensemble.RandomForestRegressor()
        self.detail = [sklearn.ensemble.RandomForestRegressor() for _ in range(self.N_CLASSES)]

        self.fitted_min = None
        self.fitted_class_range = None

    def fit(self, X, Y):
        X = np.array(X)
        Y = np.array(Y)
        # Fit the coarse estimator to provide a first level approximation
        self.coarse.fit(X, Y)

        # Split the dataset based on Y, and fit each of the X detail estimators
        self.fitted_min = label_min = Y.min()
        label_range = Y.max() - label_min + 1
        self.fitted_class_range = class_range = float(label_range) / self.N_CLASSES

        print self.fitted_min
        print self.fitted_class_range

        for i, estimator in enumerate(self.detail):
            start = int(label_min + class_range * i) - int(class_range * self.CLASS_OVERLAP)
            end = int(label_min + class_range * (i + 1)) + int(class_range * self.CLASS_OVERLAP)
            # extract ages start <= X < end
            print start
            print end
            mask = (Y < end) & (Y >= start)
            print mask
            X_dash = X[mask]
            Y_dash = Y[mask]
            print X_dash.shape

            estimator.fit(X_dash, Y_dash)

    def predict(self, X):
        X = np.array(X)
        #Y = np.array(Y)
        coarse_predictions = self.coarse.predict(X)

        label_min = self.fitted_min
        class_range = self.fitted_class_range
        output_predictions = np.zeros((X.shape[0], 1))
        for i, estimator in enumerate(self.detail):
            start = int(label_min + class_range * i)
            end = int(label_min + class_range * (i + 1))
            # extract ages start <= X < end
            mask = (coarse_predictions < end) & (coarse_predictions >= start)
            X_dash = X[mask]
            #print "Actual: ", Y[mask]
            print "Coarse: ", coarse_predictions[mask]
            output_indices = mask.nonzero()[0]
            print "Indices: ", output_indices

            if X_dash.shape[0]:
                detail_predictions = estimator.predict(X_dash)
                print "Detail: ", detail_predictions
                for i, index in enumerate(output_indices.tolist()):
                    output_predictions[index] = detail_predictions[i][0]

        return output_predictions

class AltEstimator(sklearn.base.BaseEstimator, sklearn.base.RegressorMixin):
    N_CLASSES = 4
    CLASS_OVERLAP = 0.2
    i = 0

    def __init__(self):
        # Coarse and detailed estimators
        print self.i
        AltEstimator.i += 1
        self.coarse = sklearn.svm.SVR()
        self.detail = [sklearn.svm.SVR() for _ in range(self.N_CLASSES+self.N_CLASSES-1)]

        self.fitted_min = None
        self.fitted_class_range = None

    def fit(self, X, Y):
        X = np.array(X)
        Y = np.array(Y)
        # Fit the coarse estimator to provide a first level approximation
        self.coarse.fit(X, Y)

        # Split the dataset based on Y, and fit each of the X detail estimators
        self.fitted_min = label_min = Y.min()
        label_range = Y.max() - label_min + 1
        self.fitted_class_range = class_range = float(label_range) / self.N_CLASSES


        for i in range(self.N_CLASSES):
            start = int(label_min + class_range * i)
            end = int(label_min + class_range * (i + 1))
            # extract ages start <= X < end
            mask = (Y < end) & (Y >= start)
            X_dash = X[mask]
            Y_dash = Y[mask]

            self.detail[i*2].fit(X_dash, Y_dash)

        # Now fit overlap classes
        for i in range(self.N_CLASSES-1):
            start = int(label_min + class_range * i)
            end = int(label_min + class_range * (i + 2))

            mask = (Y < end) & (Y >= start)
            X_dash = X[mask]
            Y_dash = Y[mask]

            self.detail[i*2 + 1].fit(X_dash, Y_dash)

    def predict(self, X):
        X = np.array(X)
        #Y = np.array(Y)
        coarse_predictions = self.coarse.predict(X)

        label_min = self.fitted_min
        class_range = self.fitted_class_range
        output_predictions = np.zeros((X.shape[0], 1))

        for i in range(self.N_CLASSES):
            start = int(label_min + class_range * i)
            end = int(label_min + class_range * (i + 1))
            # extract ages start <= X < end
            mask = (coarse_predictions < end) & (coarse_predictions >= start)
            X_dash = X[mask]
            output_indices = mask.nonzero()[0]

            if X_dash.shape[0]:
                detail_predictions = self.detail[i*2].predict(X_dash)
                for i, index in enumerate(output_indices.tolist()):
                    output_predictions[index] = detail_predictions[i]

        for i in range(self.N_CLASSES-1):
            centre = int(label_min + class_range * (i + 1))
            start = centre - int(class_range * self.CLASS_OVERLAP)
            end = centre + int(class_range * self.CLASS_OVERLAP)

            mask = (coarse_predictions < end) & (coarse_predictions >= start)

            X_dash = X[mask]
            output_indices = mask.nonzero()[0]

            if X_dash.shape[0]:
                detail_predictions = self.detail[i*2 + 1].predict(X_dash)
                for i, index in enumerate(output_indices.tolist()):
                    output_predictions[index] = detail_predictions[i]

        return output_predictions

def classify(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)

    features = np.array([entry['final_features'] for entry in index['faces']])
    ages = np.array([entry['age'] for entry in index['faces']])
    face_genders = np.array([genders[entry['id'][:3]] for entry in index['faces']])

    gender_estimator = sklearn.ensemble.RandomForestClassifier()
    scores = sklearn.cross_validation.cross_val_score(
        gender_estimator, features, face_genders, scoring='accuracy',
        cv=sklearn.cross_validation.KFold(len(features), n_folds=5,
                                          shuffle=True),
        n_jobs=-1
    )

    with open(join(dir_out, 'gender_scores.csv'), 'wb') as scores_file:
        scores_file.write('\n'.join(str(s) for s in scores))
        scores_file.write('\n' + str(scores.mean()) + '\n')


    test_estimator = Estimator()
    scores = sklearn.cross_validation.cross_val_score(
        test_estimator, features, ages, scoring='mean_absolute_error',
        cv=sklearn.cross_validation.LeaveOneOut(len(features)),
        n_jobs=-1
    )

    with open(join(dir_out, 'scores.csv'), 'wb') as scores_file:
        scores_file.write('\n'.join(str(s) for s in scores))
        scores_file.write('\n' + str(scores.mean()) + '\n')

    predictions = sklearn.cross_validation.cross_val_predict(
        test_estimator, features, ages,
        cv=sklearn.cross_validation.LeaveOneOut(len(features)),
        n_jobs=-1
    )

    with open(join(dir_out, 'predictions.csv'), 'wb') as predictions_file:
        predictions_file.write('\n'.join(str(a) + ',' + str(p) for a, p  in zip(ages, predictions)))

    #test_estimator.fit(features[:800], ages[:800])
    #print test_estimator.predict(features[800:], ages[800:])


if __name__ == '__main__':
    import sys
    classify(sys.argv[1], sys.argv[2])
