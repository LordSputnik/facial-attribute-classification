#!/usr/bin/env python
# -*- coding: utf8 -*-

from os.path import abspath, isdir, join

from index import load_index, save_index
from util import load_image

import numpy as np
import math
import cv2
from collections import Counter


def get_code(subwindow):
    uniform_codes = [1, 3, 7, 15, 31, 63, 127, 255]

    subwindow.flags.writeable = False
    if subwindow.data in get_code.converted:
        return get_code.converted[subwindow.data]

    results = np.sum(get_code.rolls * subwindow, axis=(1, 2))
    minimum = np.min(results)

    if minimum in uniform_codes:
        get_code.converted[subwindow.data] = uniform_codes.index(minimum)
        return uniform_codes.index(minimum)
    else:
        get_code.converted[subwindow.data] = 8
        return 8

base = np.insert(np.power(2, np.arange(0, 8)), 4, 0)
get_code.rolls = np.array([
    [base[[0, 1, 2]], base[[8, 4, 3]], base[[7, 6, 5]]],
    [base[[1, 2, 3]], base[[0, 4, 5]], base[[8, 7, 6]]],
    [base[[2, 3, 5]], base[[1, 4, 6]], base[[0, 8, 7]]],
    [base[[3, 5, 6]], base[[2, 4, 7]], base[[1, 0, 8]]],
    [base[[5, 6, 7]], base[[3, 4, 8]], base[[2, 1, 0]]],
    [base[[6, 7, 8]], base[[5, 4, 0]], base[[3, 2, 1]]],
    [base[[7, 8, 0]], base[[6, 4, 1]], base[[5, 3, 2]]],
    [base[[8, 0, 1]], base[[7, 4, 2]], base[[6, 5, 3]]],
])
get_code.converted = {}


def lbp(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)

    for i, entry in enumerate(index['faces']):
        count = [0] * 9

        image = cv2.cvtColor(load_image(entry['warped_bridge_nose']), cv2.COLOR_BGR2GRAY)
        x_size, y_size = image.shape
        for y in range(1, y_size-1):
            for x in range(1, x_size-1):
                # Get subwindow
                subwindow = image[x-1:x+2, y-1:y+2]
                # Calculate LBP
                thresholded = subwindow >= subwindow[1, 1]
                count[get_code(thresholded)] += 1

        image = cv2.cvtColor(load_image(entry['warped_left_cheek']), cv2.COLOR_BGR2GRAY)
        x_size, y_size = image.shape
        for y in range(1, y_size-1):
            for x in range(1, x_size-1):
                # Get subwindow
                subwindow = image[x-1:x+2, y-1:y+2]
                # Calculate LBP
                thresholded = subwindow >= subwindow[1, 1]
                count[get_code(thresholded)] += 1

        image = cv2.cvtColor(load_image(entry['warped_right_cheek']), cv2.COLOR_BGR2GRAY)
        x_size, y_size = image.shape
        for y in range(1, y_size-1):
            for x in range(1, x_size-1):
                # Get subwindow
                subwindow = image[x-1:x+2, y-1:y+2]
                # Calculate LBP
                thresholded = subwindow >= subwindow[1, 1]
                count[get_code(thresholded)] += 1

        entry['lbp'] = count
        print i, count

    save_index(index, dir_out)

if __name__ == '__main__':
    import sys
    lbp(sys.argv[1], sys.argv[2])
