#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script loads all previously calculated features, then concatenates
them into a single vector for each entry, and finally runs PCA over all
vectors, to produce a low dimensional discriminative output.
"""

from os.path import abspath, isdir, join

import numpy as np
import sklearn.lda
import sklearn.decomposition

from index import load_index, save_index
import math
# from mvpa2.suite import *


def _normalize(feature_vectors):
    """ Normalize a particular set of features, by subtracting the mean and
    dividing by the standard deviation.
    """

    return feature_vectors

    mean_features = np.mean(feature_vectors, axis=0)
    std_deviation = np.std(feature_vectors, axis=0)

    # Eliminate 0s, and replace with 1s
    std_deviation[std_deviation == 0] = 1

    return (feature_vectors - mean_features) / std_deviation


def features(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)

    # Normalize each group of features
    aam = _normalize(np.array([entry['aam'] for entry in index['faces']]))
    fc6 = _normalize(np.array([entry['fc6'] for entry in index['faces']]))
    fc7 = _normalize(np.array([entry['fc7'] for entry in index['faces']]))
    gabor = _normalize(np.array([entry['gabor'] for entry in index['faces']]))
    lbp = _normalize(np.array([entry['lbp'] for entry in index['faces']]))

    unified_features = np.array([
        aam[i].tolist() +
        fc6[i].tolist() +
        fc7[i].tolist()
        for i in range(len(index['faces']))
    ])

    # som = SimpleSOMMapper((10, 20), 400, learning_rate=0.05)
    # som.train(np.array(unified_features))
    # som_features = som(unified_features)

    pca = sklearn.decomposition.PCA(n_components=0.9999, copy=False, whiten=True)
    final_features = pca.fit_transform(unified_features)

    for i, entry in enumerate(index['faces']):
        entry['final_features'] = final_features[i].tolist()

    save_index(index, dir_out)


if __name__ == '__main__':
    import sys
    features(sys.argv[1], sys.argv[2])
