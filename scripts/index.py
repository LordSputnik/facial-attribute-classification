#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script reads through a directory of images and points, and builds a
datastructure (index), linking image and points filenames and ages.
"""

import json
import os
import re
from os.path import abspath, isdir, join, splitext

IMAGE_EXTENSIONS = ('.png', '.jpg')


def load_index(directory):
    if not isdir(directory):
        raise IOError("Directory does not exist!")

    index_filename = join(directory, 'index')
    with open(index_filename, 'rb') as index_file:
        index_obj = json.load(index_file)

    return index_obj


def save_index(index_obj, directory):
    if not isdir(directory):
        raise IOError("Directory does not exist!")

    index_filename = join(directory, 'index')
    with open(index_filename, 'wb') as index_file:
        json.dump(index_obj, index_file, indent=4, sort_keys=True,
                  separators=(',', ': '))


def index(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    entries = os.listdir(dir_in)

    images = {
        splitext(e)[0].lower(): e
        for e in entries if splitext(e)[1].lower() in IMAGE_EXTENSIONS
    }

    points = {
        splitext(e)[0].lower(): e
        for e in entries if splitext(e)[1].lower() == '.pts'
    }

    if len(images) != len(points):
        raise ValueError("Points/Images Mismatch!")

    index_obj = {'faces': []}
    for k in images.keys():
        age = int(re.findall(r'a(\d+)', k)[0])
        full_image_path = abspath(join(dir_in, images[k]))
        full_points_path = abspath(join(dir_in, points[k]))
        index_obj['faces'].append({
            'id': k,
            'texture': full_image_path,
            'shape': full_points_path,
            'age': age
        })

    save_index(index_obj, dir_out)

if __name__ == '__main__':
    import sys
    index(sys.argv[1], sys.argv[2])
