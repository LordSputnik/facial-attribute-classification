#!/usr/bin/env python
# -*- coding: utf8 -*-

""" This script reads warped face files and the mean warped face, and creates
a difference image for each face. It uses these difference images as the input
to a CNN, and extracts features from two fully connected layers.
"""

from os.path import abspath, isdir, join

import cv2

import caffe
from index import load_index, save_index
from util import load_image


def _chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def cnn(dir_in, dir_out):
    if not (isdir(dir_in) and isdir(dir_out)):
        raise IOError("Directories do not exist!")

    index = load_index(dir_in)
    mean_image = load_image(index['mean']['warped'])

    images = [load_image(e['warped']) for e in index['faces']]
    images = [
        cv2.subtract(image, mean_image, dtype=cv2.CV_16S)
        for image in images
    ]

    chunks = _chunks(images, 300)

    weights_filename = (
        '/home/ben/Code/caffe/models/'
        'bvlc_reference_caffenet/bvlc_reference_caffenet.caffemodel'
    )

    fc6_net = caffe.Classifier('deploy6.prototxt', weights_filename,
                               image_dims=(256, 256))

    fc7_net = caffe.Classifier('deploy7.prototxt', weights_filename,
                               image_dims=(256, 256))

    fc6_outputs = []
    fc7_outputs = []
    for chunk in chunks:
        fc6_outputs.extend(fc6_net.predict(chunk, oversample=True))
        fc7_outputs.extend(fc7_net.predict(chunk, oversample=True))

    for i, entry in enumerate(index['faces']):
        entry['fc6'] = fc6_outputs[i].tolist()
        entry['fc7'] = fc7_outputs[i].tolist()

    save_index(index, dir_out)


if __name__ == '__main__':
    import sys
    cnn(sys.argv[1], sys.argv[2])
