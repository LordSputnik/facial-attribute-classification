Facial Attribute Classification
===============================

Introduction
------------

This project will investigate approaches to automatically classifying the
attributes of faces in RGB images. Example attributes may include age, gender,
ethnicity or expression. The focus wil lbe on age classification but other
attributes could be considered depending upon the generality of the approach
and classification pipeline.

Installation
------------

Simply clone the package, and install python-opencv from your dirtribution's
package manager.
